﻿using AutoFixture;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public class SetPartnerPromoCodeLimitAsyncTestUseDataBase : IClassFixture<Fixture_InMemory>
    {
        private readonly IHost _host;
        private readonly PartnersController _partnersController;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly DataContext _dataContext;
        private readonly Fixture _fixture;
        public SetPartnerPromoCodeLimitAsyncTestUseDataBase(Fixture_InMemory fixtureInMemory)
        {
            var serviceProvider = fixtureInMemory.ServiceProvider;

            _partnerRepository = serviceProvider.GetService<IRepository<Partner>>();
            _dataContext = serviceProvider.GetService<DataContext>();
            _fixture = new Fixture();
            _partnersController = new PartnersController(_partnerRepository);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_ShouldReturnAdded_InDatabaseLimit()
        {
            //Arrange
            Guid partnerId = Guid.Parse("0DA65561-CF56-4942-BFF2-22F50CF70D43");
            var limit = 5;
            var endDate = new DateTime(2025, 3, 5);
            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = limit,
                EndDate = endDate
            };

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            var partner = await _partnerRepository.GetByIdAsync(partnerId);
            partner.PartnerLimits.Last().EndDate.Should().Be(endDate);
            partner.PartnerLimits.Last().Limit.Should().Be(limit);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IfSetNewLimit_NeedDisableOldLimit()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            var cancelPartnerPromoCodeLimit = Guid.NewGuid();
            var person = new Partner
            {
                Id = partnerId,
                IsActive = true,
                Name = "test",
                NumberIssuedPromoCodes = 12,
                PartnerLimits = new List<PartnerPromoCodeLimit>
                {
                    new PartnerPromoCodeLimit
                    {
                        Id = cancelPartnerPromoCodeLimit,
                        PartnerId = partnerId,
                        Limit = 200
                    }
                }
            };

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            _dataContext.Add(person);
            _dataContext.SaveChanges();

            //Act
            var result = _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            var partner = await _partnerRepository.GetByIdAsync(partnerId);
            partner.NumberIssuedPromoCodes.Should().Be(0); //Выставляем лимит, поэтому количество должно обнулиться
            partner.PartnerLimits.First(x => x.Id == cancelPartnerPromoCodeLimit).CancelDate.Value.Date.Should().Be(DateTime.Now.Date);
        }
    }
}
