﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NJsonSchema.Validation;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _mockPartnerRepository;
        private readonly PartnersController _partnerController;
        private readonly IFixture fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            fixture = new Fixture().Customize(new AutoMoqCustomization());

            _mockPartnerRepository = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnerController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnNotFound()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = null;

            _mockPartnerRepository.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var model = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partnerId, model);

            //Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsLock_ReturnBadRequest()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            var model = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            var partner = fixture.Build<Partner>()
                .With(p => p.IsActive, false)
                .With(p => p.Id, partnerId)
                .Without(p => p.PartnerLimits)
                .Create();
            fixture.Customize<Partner>(c => c.OmitAutoProperties());

            _mockPartnerRepository.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partnerId, model);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_LimitLessThen0_ReturnsBadRequest(int limit)
        {
            //Arrange
            var partnerLimits = fixture.Build<PartnerPromoCodeLimit>()
                .Without(p => p.Partner)
                .Without(p => p.CancelDate)
                .CreateMany()
                .ToList();

            var partner = fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.PartnerLimits, partnerLimits)
                .Create();

            var dto = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(d => d.Limit, limit)
                .Create();

            _mockPartnerRepository.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, dto);

            //Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_IfSetLimit_NumberIssuedPromoCodesIs0()
        {
            //Arrange
            var partnerId = Guid.NewGuid();

            var limit = fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.PartnerId, partnerId)
                .Without(p => p.Partner)
                .Without(p => p.CancelDate)
                .CreateMany(1)
                .ToList();

            Partner partner = fixture.Build<Partner>()
                .With(p => p.Id, partnerId)
                .With(p => p.NumberIssuedPromoCodes, 5) //Количество промокодов должно быть > 0
                .With(p => p.PartnerLimits, limit).Create();

            var dto = fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(d => d.Limit, 3).Create(); //Выставляем лимит

            _mockPartnerRepository.Setup(p => p.GetByIdAsync(partnerId)).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partnerId, dto);

            //Assert
            dto.Limit.Should().BeGreaterThan(0); //Лимит больше 0
            partner.NumberIssuedPromoCodes.Should().Be(0); // Проверка, что количество промокодов, которые партнер выдал обнулено
            partner.PartnerLimits.Count.Should().Be(limit.Count); //Новый лимит сохранен в БД. (Если бы не был сохранен, количество элементов было бы меньше) 

        }
    }
}