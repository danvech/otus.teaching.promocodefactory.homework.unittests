﻿using FluentAssertions.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
    public static class Configuration
    {

        public static IServiceCollection GetServiceCollection(IConfigurationRoot config, string serviceName, IServiceCollection serviceCollection = null)
        {
            if (serviceCollection == null)
            {
                serviceCollection = new ServiceCollection();
            }

            serviceCollection
                .AddSingleton(config)
                .AddSingleton((IConfiguration)config)
                .ConfigureAllBisnessServices()
                .AddMemoryCache();

            return serviceCollection;
        }


        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection serviceCollection)
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .AddEntityFrameworkProxies()
                .BuildServiceProvider();
            serviceCollection.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
                options.UseLazyLoadingProxies();
            });
            serviceCollection.AddTransient<DbContext, DataContext>();

            return serviceCollection;
        }

        private static IServiceCollection ConfigureAllBisnessServices(this IServiceCollection services) => services
            .AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
    }
}
