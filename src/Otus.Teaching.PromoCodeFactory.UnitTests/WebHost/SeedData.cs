﻿using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
    public static class SeedData
    {
        public static void SeedFakeData(this IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = scope.ServiceProvider.GetService<DataContext>();

                context.AddRange(FakeDataFactory.Employees);
                context.AddRange(FakeDataFactory.Partners);
                context.AddRange(FakeDataFactory.Customers);
                context.AddRange(FakeDataFactory.Preferences);

                context.SaveChanges();

            }
        }
    }
}
